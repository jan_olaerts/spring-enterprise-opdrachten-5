package be.janolaerts.enterprisespringopdrachten.exceptions;

public class InvalidAmountException extends RuntimeException {

    public InvalidAmountException(String message) {
        System.err.println(message);
    }
}