package be.janolaerts.enterprisespringopdrachten.exceptions;

public class InvalidBeerException extends RuntimeException {

    public InvalidBeerException(Throwable cause) {
        super(cause);
    }

    public InvalidBeerException(String message) {
        System.err.println(message);
    }
}