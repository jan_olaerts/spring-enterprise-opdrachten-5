package be.janolaerts.enterprisespringopdrachten.services;

import be.janolaerts.enterprisespringopdrachten.entity.Beer;
import be.janolaerts.enterprisespringopdrachten.exceptions.InvalidBeerException;
import be.janolaerts.enterprisespringopdrachten.exceptions.InvalidAmountException;
import be.janolaerts.enterprisespringopdrachten.repositories.BeerOrderRepository;
import be.janolaerts.enterprisespringopdrachten.repositories.BeerRepository;

import java.util.List;

public interface BeerService {

    Beer getBeerById(int id);
    void updateBeer(Beer beer);
    List<Beer> getBeersByAlcohol(float alcohol);
    List<Beer> findBeersByNameLikeOrderByNameAsc(String name);
    List<Beer> findBeersByStockLessThan(int stock);
    void updatePrice(float percentage);
    List<Beer> findBeersWithAlcohol9AndNameStartsWithSt();
    void setStock(int stock, int id);
    int orderBeer(String name, int beerId, int number) throws InvalidBeerException, InvalidAmountException;
    int orderBeers(String name, int[][] order) throws InvalidAmountException, InvalidBeerException;
}