package be.janolaerts.enterprisespringopdrachten.dao;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@Repository("beerDao")
public class BeerDaoImpl implements BeerDao {

    private final static String QUERY_ID =
            "SELECT Name, Alcohol, Price, Stock FROM Beers WHERE Id=?";

    private final static String UPDATE_STOCK =
            "UPDATE Beers SET Stock=? WHERE Id=?";

    private final static String GET_BY_ALCOHOL =
            "SELECT Name, Alcohol, Price, Stock FROM Beers WHERE Alcohol=?";

    @Autowired
    private JdbcTemplate template;

    @Override
    public String getBeerById(int id) {
        if(id < 0) throw new IllegalArgumentException("id cannot be smaller than 0");

        Map<String, Object> result =
                template.queryForMap(QUERY_ID, id);

        return String.format("%s %s %s %s",
                result.get("name"),
                result.get("alcohol"),
                result.get("price"),
                result.get("stock"));
    }

    @Override
    public void setStock(int id, int stock) {
        if(id < 0) throw new IllegalArgumentException("id cannot be smaller than 0");
        if(stock < 0) throw new IllegalArgumentException("stock cannot be smaller than 0");

        template.update(UPDATE_STOCK, stock, id);
    }

    @Override
    public List<String> getBeersByAlcohol(float alcohol) {

        List<String> beers = new ArrayList<>();
        List<Map<String, Object>> rows = template.queryForList(GET_BY_ALCOHOL, alcohol);

        rows.forEach(row -> beers.add(row.toString()));

        return beers;
    }
}