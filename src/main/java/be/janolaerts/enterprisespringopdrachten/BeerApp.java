package be.janolaerts.enterprisespringopdrachten;

import be.janolaerts.enterprisespringopdrachten.entity.Beer;
import be.janolaerts.enterprisespringopdrachten.entity.Brewer;
import be.janolaerts.enterprisespringopdrachten.entity.Category;
import be.janolaerts.enterprisespringopdrachten.repositories.BeerOrderRepository;
import be.janolaerts.enterprisespringopdrachten.repositories.BeerRepository;
import be.janolaerts.enterprisespringopdrachten.repositories.BrewerRepository;
import be.janolaerts.enterprisespringopdrachten.repositories.CategoryRepository;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;

import java.util.List;

@SpringBootApplication
public class BeerApp {

    // Todo: ask Ward -> App does not stop automatically

    public static void main(String[] args) {

        ConfigurableApplicationContext ctx =
                SpringApplication.run(BeerApp.class, args);

        BeerRepository beerRepository = ctx.getBean("beerRepository", BeerRepository.class);
        BeerOrderRepository beerOrderRepository = ctx.getBean("beerOrderRepository", BeerOrderRepository.class);
        BrewerRepository brewerRepository = ctx.getBean("brewerRepository", BrewerRepository.class);
        CategoryRepository categoryRepository = ctx.getBean("categoryRepository", CategoryRepository.class);

        // Opdracht 5
//        List<Beer> beers = beerRepository.findBeersWithAlcohol9AndNameStartsWithSt();
//        beers.forEach(System.out::println);

//        Beer beer = beerRepository.getBeerById(15);
//        System.out.println(beer);

//        Brewer brewer = brewerRepository.getBrewerById(25);
//        System.out.println(brewer);
//
//        Category category = categoryRepository.getCategoryById(5);
//        System.out.println(category);

//        List<Brewer> allBrewers = brewerRepository.findAll();
//        allBrewers.forEach(System.out::println);

//        List<Category> allCategories = categoryRepository.findAll();
//        allCategories.forEach(System.out::println);
    }
}