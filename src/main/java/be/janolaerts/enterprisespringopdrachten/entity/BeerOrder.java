package be.janolaerts.enterprisespringopdrachten.entity;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name="BeerOrders")
@NamedQueries(value={
        @NamedQuery(name="getBeerOrderById", query="select bo from BeerOrder bo where bo.id=?1")
})
public class BeerOrder {

    @Id
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    @Column(name="Id")
    private int id;

    @Column(name="Name")
    private String name;

    @OneToMany(cascade={CascadeType.ALL}, fetch=FetchType.EAGER)
    @JoinColumn(name="BeerOrderId")
    private List<BeerOrderItem> items = new ArrayList<>();

    public BeerOrder() {
    }

    public BeerOrder(String name, List<BeerOrderItem> items) {
        this.name = name;
        this.items = items;
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<BeerOrderItem> getItems() {
        return items;
    }

    public void setItems(List<BeerOrderItem> items) {
        this.items = items;
    }

    @Override
    public String toString() {
        return "BeerOrder{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", items=" + items +
                '}';
    }
}