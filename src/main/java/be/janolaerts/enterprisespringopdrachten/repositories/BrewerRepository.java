package be.janolaerts.enterprisespringopdrachten.repositories;

import be.janolaerts.enterprisespringopdrachten.entity.Brewer;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.stereotype.Component;

import javax.transaction.Transactional;

@Component
public interface BrewerRepository extends JpaRepository<Brewer, Long> {

    @Transactional
    @Modifying
    public default void saveBrewer(Brewer brewer) {
        saveAndFlush(brewer);
    }

    @Transactional
    public default Brewer getBrewerById(long id) {
        return findById(id).orElse(null);
    }

    @Transactional
    @Modifying
    public default void updateBrewer(Brewer brewer) {
        save(brewer);
    }

    @Transactional
    @Modifying
    void deleteById(long id);
}