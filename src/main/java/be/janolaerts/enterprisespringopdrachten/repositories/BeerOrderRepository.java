package be.janolaerts.enterprisespringopdrachten.repositories;

import be.janolaerts.enterprisespringopdrachten.entity.BeerOrder;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;

import javax.transaction.Transactional;

public interface BeerOrderRepository extends JpaRepository<BeerOrder, Integer> {

    @Transactional
    BeerOrder getBeerOrderById(int id);

    @Transactional
    @Modifying
    public default int saveOrder(BeerOrder order) {
        order = save(order);
        return order.getId();
    }
}