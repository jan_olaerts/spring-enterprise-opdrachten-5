package be.janolaerts.enterprisespringopdrachten.repositories;

import be.janolaerts.enterprisespringopdrachten.entity.Beer;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.domain.Example;
import org.springframework.data.domain.ExampleMatcher;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Component;

import javax.transaction.Transactional;
import java.util.List;

@Component
public interface BeerRepository extends JpaRepository<Beer, Integer> {

    @Transactional
    public default Beer getBeerById(int id) {
        return findById(id).orElse(null);
    }

    @Transactional
    public default void updateBeer(Beer beer) {
        save(beer);
    }

    @Transactional
    @Query(name="findByAlcohol")
    List<Beer> getBeersByAlcohol(float alcohol);

    // Todo: ask Ward: Unable to put this query as NamedQuery
    @Transactional
    @Query("select b from Beer b where b.name like %?1%")
    List<Beer> findBeersByNameLikeOrderByNameAsc(String name);

    @Transactional
    List<Beer> findBeersByStockLessThan(int stock);

    @Transactional
    @Modifying
    @Query(name="updateBeerPricesWithPercentage")
    void updatePrice(double percentage);

    // Find beers with alcohol 9 and name starts with 'st'
    // Todo: ask War: Not sure about this one (start with 'St')
    public default List<Beer> findBeersWithAlcohol9AndNameStartsWithSt() {
        ExampleMatcher matcher = ExampleMatcher.matchingAny()
                                               .withIgnorePaths("id", "brewer", "category", "price", "stock", "version", "image")
                                               .withMatcher("beer.name", ExampleMatcher.GenericPropertyMatcher::startsWith);

        Beer probe = new Beer();
        probe.setAlcohol(9f);
        probe.setName("St");
        Example<Beer> example = Example.of(probe, matcher);
        return findAll(example);
    }

    @Transactional
    @Modifying
    @Query(name="updateStockById")
    void setStock(@Param("stock") int stock, @Param("id") int id);
}