package be.janolaerts.enterprisespringopdrachten.repositories;

import be.janolaerts.enterprisespringopdrachten.entity.Category;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.stereotype.Component;

import javax.transaction.Transactional;

@Component
public interface CategoryRepository extends JpaRepository<Category, Long> {

    @Transactional
    @Modifying
    public default void saveCategory(Category category) {
        saveAndFlush(category);
    }

    @Transactional
    public default Category getCategoryById(long id) {
        return findById(id).orElse(null);
    }

    @Transactional
    @Modifying
    public default void updateCategory(Category category) {
        save(category);
    }

    @Transactional
    @Modifying
    void deleteById(long id);
}