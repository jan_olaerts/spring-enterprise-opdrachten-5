package be.janolaerts.enterprisespringopdrachten.repositories;

import be.janolaerts.enterprisespringopdrachten.entity.Brewer;
import be.janolaerts.enterprisespringopdrachten.entity.Category;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.test.annotation.Commit;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import javax.transaction.Transactional;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

@ExtendWith(SpringExtension.class)
@SpringBootTest
class CategoryRepositoryTest {

    @Autowired
    private CategoryRepository categoryRepository;

    @Test
    void saveCategory() {
        Category category = new Category("SavedCategory");
        categoryRepository.saveCategory(category);
        List<Category> categories = categoryRepository.findAll();
        assertEquals(3, categories.size());
        assertEquals("SavedCategory", categories.get(2).getName());
    }

    @Test
    void findAll() {
        List<Category> categories = categoryRepository.findAll();
        assertEquals(2, categories.size());
    }

    @Test
    void getCategoryById() {
        Category category;

        category = categoryRepository.getCategoryById(3L);
        assertNull(category);

        category = categoryRepository.getCategoryById(2L);
        assertEquals("TestCategory2", category.getName());
    }

    @Test
    void updateCategory() {
        Category category;

        category = categoryRepository.getCategoryById(1L);
        category.setName("UpdatedCategory");
        categoryRepository.updateCategory(category);

        category = categoryRepository.getCategoryById(1L);
        assertEquals("UpdatedCategory", category.getName());
    }

    @Test
    void deleteById() {
        categoryRepository.deleteById(2L);
        assertNull(categoryRepository.getCategoryById(2L));
    }
}