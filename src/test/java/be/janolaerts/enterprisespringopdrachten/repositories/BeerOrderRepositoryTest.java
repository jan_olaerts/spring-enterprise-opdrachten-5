package be.janolaerts.enterprisespringopdrachten.repositories;

import be.janolaerts.enterprisespringopdrachten.entity.BeerOrder;
import be.janolaerts.enterprisespringopdrachten.entity.BeerOrderItem;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

@ExtendWith(SpringExtension.class)
@SpringBootTest
class BeerOrderRepositoryTest {

    @Autowired
    private BeerRepository beerRepository;

    @Autowired
    private BeerOrderRepository beerOrderRepository;

    @Test
    void getBeerOrderById() {
        BeerOrder beerOrder;

        beerOrder = beerOrderRepository.getBeerOrderById(4);
        assertNull(beerOrder);

        beerOrder = beerOrderRepository.getBeerOrderById(1);
        assertEquals("Order 1", beerOrder.getName());
    }

    @Test
    void saveOrder() {
        BeerOrder order;

        order = new BeerOrder(
                "Order 3", List.of(
                new BeerOrderItem(beerRepository.getBeerById(1), 1),
                new BeerOrderItem(beerRepository.getBeerById(2), 2),
                new BeerOrderItem(beerRepository.getBeerById(3), 3),
                new BeerOrderItem(beerRepository.getBeerById(4), 4),
                new BeerOrderItem(beerRepository.getBeerById(1), 5)
        ));
        beerOrderRepository.saveOrder(order);

        order = beerOrderRepository.getBeerOrderById(3);
        assertEquals(3, order.getId());
        assertEquals("Order 3", order.getName());
        assertEquals("BeerOrder{id=3, name='Order 3', items=[BeerOrderItem{id=7, beer=Beer{id=1, name='TestBeer1', brewer=Brewer{id=1, name='TestBrewer', " +
                        "address='TestStreet', zipCode='1000', city='TestCity', turnover=10000}, category=Category{id=1, name='TestCategory'}, price=2.75, stock=100, " +
                        "alcohol=7.0, version=0, image=null}, number=1}, BeerOrderItem{id=8, beer=Beer{id=2, name='TestBeer2', brewer=Brewer{id=1, name='TestBrewer', " +
                        "address='TestStreet', zipCode='1000', city='TestCity', turnover=10000}, category=Category{id=1, name='TestCategory'}, price=5.85, stock=50, " +
                        "alcohol=8.0, version=0, image=null}, number=2}, BeerOrderItem{id=9, beer=Beer{id=3, name='TestBeer3', brewer=Brewer{id=1, name='TestBrewer', " +
                        "address='TestStreet', zipCode='1000', city='TestCity', turnover=10000}, category=Category{id=1, name='TestCategory'}, price=3.65, stock=50, " +
                        "alcohol=2.0, version=0, image=null}, number=3}, BeerOrderItem{id=10, beer=Beer{id=4, name='Star Beer', brewer=Brewer{id=1, name='TestBrewer', " +
                        "address='TestStreet', zipCode='1000', city='TestCity', turnover=10000}, category=Category{id=1, name='TestCategory'}, price=3.65, stock=50, " +
                        "alcohol=9.0, version=0, image=null}, number=4}, BeerOrderItem{id=11, beer=Beer{id=1, name='TestBeer1', brewer=Brewer{id=1, name='TestBrewer', " +
                        "address='TestStreet', zipCode='1000', city='TestCity', turnover=10000}, category=Category{id=1, name='TestCategory'}, price=2.75, stock=100, " +
                        "alcohol=7.0, version=0, image=null}, number=5}]}",
                order.toString());
    }
}