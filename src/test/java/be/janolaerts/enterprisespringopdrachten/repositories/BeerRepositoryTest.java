package be.janolaerts.enterprisespringopdrachten.repositories;

import be.janolaerts.enterprisespringopdrachten.entity.Beer;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.jpa.repository.Query;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

@ExtendWith(SpringExtension.class)
@SpringBootTest
class BeerRepositoryTest {

    @Autowired
    private BeerRepository beerRepository;

    @Test
    void getBeerById() {
        assertNull(beerRepository.getBeerById(5));

        Beer beer = beerRepository.getBeerById(1);
        assertEquals("TestBeer1", beer.getName());
    }

    @Test
    void updateBeer() {
        Beer beer;

        beer = beerRepository.getBeerById(1);
        beer.setName("UpdatedBeer");
        beerRepository.updateBeer(beer);

        beer = beerRepository.getBeerById(1);
        assertEquals("UpdatedBeer", beer.getName());
    }

    @Test
    void getBeersByAlcohol() {
        List<Beer> beers;

        beers = beerRepository.getBeersByAlcohol(3f);
        assertEquals(0, beers.size());

        beers = beerRepository.getBeersByAlcohol(2f);
        assertEquals(1, beers.size());
    }

    // Todo: ask Ward: This test fails when running all tests (@Rollback on update name test does not work)
    @Test
    void findBeersByNameLikeOrderByNameAsc() {
        List<Beer> beers;

        beers = beerRepository.findBeersByNameLikeOrderByNameAsc("TestBeer4");
        assertEquals(0, beers.size());

        beers = beerRepository.findBeersByNameLikeOrderByNameAsc("Test");
        assertEquals(3, beers.size());
        assertEquals("TestBeer1", beers.get(0).getName());
    }

    @Test
    void findBeersByStockLessThan() {
        List<Beer> beers;

        beers = beerRepository.findBeersByStockLessThan(40);
        assertEquals(0, beers.size());

        beers = beerRepository.findBeersByStockLessThan(60);
        assertEquals(3, beers.size());

        boolean test = false;
        for(Beer beer : beers) {
            if(beer.getName().equals("TestBeer2")) {
                test = true;
                break;
            }
        }

        assertTrue(test);
    }

    @Test
    void updatePrice() {
        Beer beer;
        beerRepository.updatePrice(2d);

        beer = beerRepository.getBeerById(1);
        assertEquals(5.5, beer.getPrice());

        beer = beerRepository.getBeerById(2);
        assertEquals(11.7, beer.getPrice());

        beer = beerRepository.getBeerById(3);
        assertEquals(7.3, beer.getPrice());
    }

    @Test
    void findBeersWithAlcohol9AndNameStartsWithSt() {
        List<Beer> beers = beerRepository.findBeersWithAlcohol9AndNameStartsWithSt();
        assertEquals(1, beers.size());
    }

    @Test
    void setStock() {
        beerRepository.setStock(200, 1);
        assertEquals(200, beerRepository.getBeerById(1).getStock());
    }
}