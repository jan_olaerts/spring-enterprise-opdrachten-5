package be.janolaerts.enterprisespringopdrachten.repositories;

import be.janolaerts.enterprisespringopdrachten.entity.Brewer;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

@ExtendWith(SpringExtension.class)
@SpringBootTest
class BrewerRepositoryTest {

    @Autowired
    private BrewerRepository brewerRepository;

    @Test
    void saveBrewer() {
        Brewer brewer = new Brewer("SavedBrewer", "BrewerAddress", "1000", "BrewerCity", 1000L);
        brewerRepository.saveBrewer(brewer);
        List<Brewer> brewers = brewerRepository.findAll();
        assertEquals(3, brewers.size());
        assertEquals("SavedBrewer", brewers.get(2).getName());
    }

    @Test
    void findAll() {
        List<Brewer> brewers = brewerRepository.findAll();
        assertEquals(2, brewers.size());
    }

    @Test
    void getBrewerById() {
        Brewer brewer;

        brewer = brewerRepository.getBrewerById(3);
        assertNull(brewer);

        brewer = brewerRepository.getBrewerById(2);
        assertEquals("TestBrewer2", brewer.getName());
    }

    @Test
    void updateBrewer() {
        Brewer brewer;

        brewer = brewerRepository.getBrewerById(1L);
        brewer.setName("UpdatedBrewer");
        brewerRepository.updateBrewer(brewer);

        brewer = brewerRepository.getBrewerById(1L);
        assertEquals("UpdatedBrewer", brewer.getName());
    }

    @Test
    void deleteById() {
        brewerRepository.deleteById(2L);
        assertNull(brewerRepository.getBrewerById(2L));
    }
}