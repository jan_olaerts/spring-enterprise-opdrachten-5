TRUNCATE TABLE Brewers;
TRUNCATE TABLE Categories;
TRUNCATE TABLE Beers;
TRUNCATE TABLE BeerOrders;
TRUNCATE TABLE BeerOrderItems;

INSERT INTO Brewers VALUES (1,'TestBrewer1','TestStreet',1000,'TestCity',10000);
INSERT INTO Brewers VALUES (2,'TestBrewer2','TestStreet',1000,'TestCity',10000);

INSERT INTO Categories VALUES (1,'TestCategory1');
INSERT INTO Categories VALUES (2,'TestCategory2');

INSERT INTO Beers VALUES (1,'TestBeer1',1,1,2.75,100,7,0,NULL);
INSERT INTO Beers VALUES (2,'TestBeer2',1,1,5.85,50,8,0,NULL);
INSERT INTO Beers VALUES (3,'TestBeer3',1,1,3.65,50,2,0,NULL);
INSERT INTO Beers VALUES (4,'Star Beer',1,1,3.65,50,9,0,NULL);

INSERT INTO BeerOrders VALUES (1, 'Order 1');
INSERT INTO BeerOrders VALUES (2, 'Order 2');

INSERT INTO BeerOrderItems VALUES (1, 1, 1, 1);
INSERT INTO BeerOrderItems VALUES (2, 1, 3, 2);
INSERT INTO BeerOrderItems VALUES (3, 1, 2, 3);

INSERT INTO BeerOrderItems VALUES (4, 2, 3, 1);
INSERT INTO BeerOrderItems VALUES (5, 2, 1, 2);
INSERT INTO BeerOrderItems VALUES (6, 2, 2, 3);